from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import ThreadedFTPServer  # <-
from pyftpdlib.authorizers import DummyAuthorizer


def main():
    authorizer = DummyAuthorizer()
    ###  only change the first three parameters
    authorizer.add_user('user', '12345', './Documents', perm='elradfmwM')
    handler = FTPHandler
    handler.authorizer = authorizer
    ###please leave the port, for consistency
    server = ThreadedFTPServer(('', 2121), handler)
    server.serve_forever()

if __name__ == "__main__":
    main()

"""TODO"""
#create bash script

#create GTK Window as menu
#options:
###start/stop server
###file manager (sunflower)
###StreamAudio
###ScreenCast
