#!/bin/bash
clear

sudo -H sh dependencies.sh

/usr/bin/sunflower &
python2 ftpserver.py &

clear

echo "===================================================
Welcome to Live Presence v0.00.1 codename: kintungi
===================================================

INSTRUCTIONS
Tools>Mount manager> + >
enter your session credentials then select 'Mount'

for local testing,
Name: pyftpdlib
Server: 0.0.0.0:2121
Directory: /
Username: user
Password: 12345"


read -n1 -r -p "Press 'q' to shutdown platform from terminal...
" key

if [[ "$key" = "q" ]]
then
	sudo kill `sudo lsof -t -i:2121`
	kill -- -0
fi
